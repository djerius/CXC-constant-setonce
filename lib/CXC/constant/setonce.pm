package CXC::constant::setonce;

# ABSTRACT: Initialize a constant subroutine once.

use v5.10;

use strict;
use warnings;

our $VERSION = '0.02';
sub import {
    my ( undef, @names ) = @_;
    my $target = caller;

    ## no critic ( TestingAndDebugging::ProhibitNoStrict)
    no strict 'refs';
    for my $name ( @names ) {

        my $fqdn = join q{::}, $target, $name;

        *{$fqdn} = sub {
            state $stored = do {
                die( "$fqdn: used before initialization" )
                  unless @_;
                shift;
            };
            @_ && die( "$fqdn: too many arguments" );
            return $stored;
        };
    }
}


1;

# COPYRIGHT

__END__


=head1 SYNOPSIS

   use CXC::constant::setonce 'MYCONST';

   # this will die, as MYCONST hasn't been set:
   say MYCONST;

   # this sets MYCONST's value;
   MYCONST( $value );

   # this will succeed
   say MYCONST;

   # and this will die, as MYCONST has already been set
   MYCONST( $value );


=head1 DESCRIPTION

This module was written for the case where

=over

=item 1

a constant's value cannot be set until run time

=item 2

attempts to use the constant before its value is set should be fatal.

=item 3

attempts to set the constant's value more than once should be fatal

=back


=head1 USAGE

=over

=item 1

Use C<CXC::constant::setonce> as a pragma to declare the constants:

  use CXC::constant::setonce qw( CONST1 CONST 2);

=item 2

Call a constant as a subroutine with an argument to set it:

  CONST1( $value );
  CONST2( $value );

=item 3

Use the constant as you would any other constant:

  if ( CONST1 == 33 ) { ... }

=back

=head1 CAVEATS

=over

=item *

If the constant is used prior to being set, any later
attempt to set the constant will result in an exception, e.g.

  use CXC::constant::setonce 'CONST';
  eval { CONST }; # will die, eval prevents that from propagating
  CONST($value)   # will die

=item *

There's no way to check if the constant has been set before using it.

=back

=head1 SEE ALSO

constant

constant::defer

