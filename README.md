# NAME

CXC::constant::setonce - Initialize a constant subroutine once.

# VERSION

version 0.02

# SYNOPSIS

    use CXC::constant::setonce 'MYCONST';

    # this will die, as MYCONST hasn't been set:
    say MYCONST;

    # this sets MYCONST's value;
    MYCONST( $value );

    # this will succeed
    say MYCONST;

    # and this will die, as MYCONST has already been set
    MYCONST( $value );

# DESCRIPTION

This module was written for the case where

1. a constant's value cannot be set until run time
2. attempts to use the constant before its value is set should be fatal.
3. attempts to set the constant's value more than once should be fatal

# USAGE

1. Use `CXC::constant::setonce` as a pragma to declare the constants:

        use CXC::constant::setonce qw( CONST1 CONST 2);

2. Call a constant as a subroutine with an argument to set it:

        CONST1( $value );
        CONST2( $value );

3. Use the constant as you would any other constant:

        if ( CONST1 == 33 ) { ... }

# CAVEATS

- If the constant is used prior to being set, any later
attempt to set the constant will result in an exception, e.g.

        use CXC::constant::setonce 'CONST';
        eval { CONST }; # will die, eval prevents that from propagating
        CONST($value)   # will die

- There's no way to check if the constant has been set before using it.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-constant-setonce@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-constant-setonce](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-constant-setonce)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-constant-setonce

and may be cloned from

    https://gitlab.com/djerius/cxc-constant-setonce.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [constant](https://metacpan.org/pod/constant)
- [constant::defer](https://metacpan.org/pod/constant%3A%3Adefer)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2023 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
